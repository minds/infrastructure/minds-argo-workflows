# Minds Argo Workflows

This repo contains the Argo Workflows resources for automation workflows used by the Minds infrastructure team.

See [this](https://argoproj.github.io/argo-workflows/quick-start/) guide for getting stareted with Argo Workflows.

## Structure

- `rbac` - contains Kubernetes RBAC policies giving Argo access to the needed resources
- `templates` - contains [WorkflowTemplate](https://argoproj.github.io/argo-workflows/workflow-templates/) resources for reusable templates
- `workflows` - contains [Workflow](https://argoproj.github.io/argo-workflows/walk-through/hello-world/) resources for defining Argo workflows
- `cronworkflows` - contains [CronWorkflow](https://argoproj.github.io/argo-workflows/cron-workflows/) for scheduled workflows

## Deploying

For testing workflows locally, you can deploy a local Kubernetes cluster and deploy the templates:

```bash
kubectl apply -f templates/
```

Then, you can deploy the workflows you are testing with `kubectl` directly:

```bash
kubectl create -f workflows/validate-vitess-backups.yml
```
